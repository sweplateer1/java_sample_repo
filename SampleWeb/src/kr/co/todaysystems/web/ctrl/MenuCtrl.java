﻿package kr.co.todaysystems.web.ctrl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Menu Controller
 * @author Ssuny_Sun
 *
 * 2023-02-08 edit1
 */
@Controller
public class MenuCtrl {

	private final Logger logger = Logger.getLogger(getClass());

	@RequestMapping(value="/menu", method=RequestMethod.GET)
	public void menu() throws Exception {
		logger.debug("Menu");
	}
}
