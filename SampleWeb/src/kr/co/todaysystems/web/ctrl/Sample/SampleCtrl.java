package kr.co.todaysystems.web.ctrl.Sample;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Sample Controller
 * @author Ssuny_Sun
 *
 */
@Controller
@RequestMapping("/Sample/*")
public class SampleCtrl {

	private final Logger logger = Logger.getLogger(getClass());

	@RequestMapping(value="/sample", method=RequestMethod.GET)
	public void sample() throws Exception {
		logger.debug("Sample");
	}
}