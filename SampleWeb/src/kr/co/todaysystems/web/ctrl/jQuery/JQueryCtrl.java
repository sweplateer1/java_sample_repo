package kr.co.todaysystems.web.ctrl.jQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * jQuery Controller
 * @author Ssuny_Sun
 *
 */
@Controller
@RequestMapping("/jQuery/*")
public class JQueryCtrl {

	private final Logger logger = Logger.getLogger(getClass());

	@RequestMapping(value="/jQueryUISample", method=RequestMethod.GET)
	public void jQueryUISample() throws Exception {
		logger.debug("jQuery UI Sample");
	}
}