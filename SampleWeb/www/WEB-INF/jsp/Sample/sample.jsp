<%@ page language='java' contentType='text/html; charset=UTF-8' pageEncoding='UTF-8'%>
<%@ taglib prefix='c' uri='/WEB-INF/tld/c.tld' %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Sample</title>

<meta http-equiv='expires' content='0'/>
<meta http-equiv='pragma' content='no-cache'/>
<meta http-equiv='cache-control' content='no-store'/>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
<meta http-equiv='X-UA-Compatible' content='IE=Edge'/>

<link type='text/css' href='<%=request.getContextPath()%>/css/common-style.css' rel='stylesheet'/>
<link type='text/css' href='<%=request.getContextPath()%>/css/jquery/jquery-ui-1.10.3.custom.css' rel='stylesheet'/>
<link type='text/css' href='<%=request.getContextPath()%>/css/jquery/jquery-ui.grid.css' rel='stylesheet'/>
<link type='text/css' href='<%=request.getContextPath()%>/css/jquery/layout-default-latest.css' rel='stylesheet'/>

<script type='text/javascript' src='<%=request.getContextPath()%>/js/jquery/jquery-1.10.2.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/js/jquery/jquery-ui-1.10.3.custom.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/js/tree/jquery.jstree.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/js/common.js'></script>

<script type='text/javascript'>

$(document).ready(function() {

});
</script>
</head>
<body>
<h1>Sample</h1>
</body>
</html>