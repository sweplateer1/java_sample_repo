<%@ page language='java' contentType='text/html; charset=UTF-8' pageEncoding='UTF-8'%>
<%@ taglib prefix='c' uri='/WEB-INF/tld/c.tld' %>
<%@ taglib uri='http://www.springframework.org/tags' prefix='spring'%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Test</title>

<meta http-equiv='expires' content='0'/>
<meta http-equiv='pragma' content='no-cache'/>
<meta http-equiv='cache-control' content='no-store'/>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
<meta http-equiv='X-UA-Compatible' content='IE=Edge'/>

<link type='text/css' href='<%=request.getContextPath()%>/css/common-style.css' rel='stylesheet'/>
<link type='text/css' href='<%=request.getContextPath()%>/css/jquery/jquery-ui-1.10.3.custom.css' rel='stylesheet'/>
<link type='text/css' href='<%=request.getContextPath()%>/css/jquery/jquery-ui.grid.css' rel='stylesheet'/>

<script type='text/javascript' src='<%=request.getContextPath()%>/js/jquery/jquery-1.10.2.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/js/jquery/jquery-ui-1.10.3.custom.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/js/jquery/jquery.cookie.js'></script>
<script type='text/javascript' src='<%=request.getContextPath()%>/js/common.js'></script>

<script type='text/javascript'>

<!-- Bitbucket Cloud Test -->


$(document).ready(function() {

	$('.table-common-list').styleTable();

	$('a[id=url-but]').on('click', function() {
		var url = $(this).attr('url');
		$(location).attr('href', url);
	});
});
</script>
</head>

<body>

<div class='ui-grid'>
	<table class='table-common-list'>
		<tr>
			<th>구분</th>
			<th>URL</th>
		</tr>
		<tr>
			<td>Samplet</td>
			<td><a id='url-but' class='link' title='Sample' url='<%=request.getContextPath()%>/Sample/sample'><%=request.getContextPath()%>/Sample/sample</a></td>
		</tr>
		<tr>
			<td>jQuery UI Samplet</td>
			<td><a id='url-but' class='link' title='jQuery UI Sample' url='<%=request.getContextPath()%>/jQuery/jQueryUISample'><%=request.getContextPath()%>/jQuery/jQueryUISample</a></td>
		</tr>
	</table>
</div>
</body>
</html>