/* Common */
$(document).ready(function() {

	/* AJAX Loading */
	$(document).ajaxStart( function() {
		$("#loading").show();
	}).ajaxStop( function() {
		$("#loading").hide();
	});

	$(':button').button();
	$('.reversal').addClass('ui-state-active');
	$('.reversal').hover( 
		function () { 
			$(this).removeClass('ui-state-active');
		},
		function () { 
			$(this).addClass('ui-state-active'); 
		}
	);

	$('.readonly').attr('readonly', true);
	$('.disabled').attr('disabled', true);
	$('.msg').attr('readonly', true);

	$('#topCheck').on('click', function() {

		var isChecked = $(this).prop('checked');
		if (isChecked) {
			$('input[id=idxCheck]').each(function () {
				$(this).attr('checked', 'checked');
				$(this).parent().parent().children('td').addClass('ui-state-highlight');
			});
		} else {
			$('input[id=idxCheck]').each(function () {
				$(this).removeAttr('checked');
				$(this).parent().parent().children('td').removeClass('ui-state-highlight');
			});
		}
	});

	$('input[id=idxCheck]').on('click', function() {

		var isTopCheck = true;
		var isChecked = $(this).prop('checked');
		var isTopChecked = $('#topCheck').prop('checked');
		if (isChecked) {
			$('input[id=idxCheck]').each(function () {
				if (!$(this).prop('checked')) {
					isTopCheck = false;
				}
			});

			if (isTopCheck) {
				$('#topCheck').attr('checked', 'checked');
			}

		} else {
			if (isTopChecked) {
				$('#topCheck').removeAttr('checked');
			}
		}

		if ($(this).prop('checked')) {
			$(this).parent().parent().children('td').addClass('ui-state-highlight');
		} else {
			$(this).parent().parent().children('td').removeClass('ui-state-highlight');
		}
	});

	$('#dialog-topCheck').on('click', function() {

		var isChecked = $(this).prop('checked');
		if (isChecked) {
			$('input[id=dialog-idxCheck]').each(function () {
				$(this).attr('checked', 'checked');
				$(this).parent().parent().children('td').addClass('ui-state-highlight');
			});
		} else {
			$('input[id=dialog-idxCheck]').each(function () {
				$(this).removeAttr('checked');
				$(this).parent().parent().children('td').removeClass('ui-state-highlight');
			});
		}
	});

	$('input[id=dialog-idxCheck]').on('click', function() {

		var isTopCheck = true;
		var isChecked = $(this).prop('checked');
		var isTopChecked = $('#dialog-topCheck').prop('checked');
		if (isChecked) {
			$('input[id=dialog-idxCheck]').each(function () {
				if (!$(this).prop('checked')) {
					isTopCheck = false;
				}
			});

			if (isTopCheck) {
				$('#dialog-topCheck').attr('checked', 'checked');
			}

		} else {
			if (isTopChecked) {
				$('#dialog-topCheck').removeAttr('checked');
			}
		}

		if ($(this).prop('checked')) {
			$(this).parent().parent().children('td').addClass('ui-state-highlight');
		} else {
			$(this).parent().parent().children('td').removeClass('ui-state-highlight');
		}
	});

	$('#sub-dialog-topCheck').on('click', function() {

		var isChecked = $(this).prop('checked');
		if (isChecked) {
			$('input[id=sub-dialog-idxCheck]').each(function () {
				$(this).attr('checked', 'checked');
				$(this).parent().parent().children('td').addClass('ui-state-highlight');
			});
		} else {
			$('input[id=sub-dialog-idxCheck]').each(function () {
				$(this).removeAttr('checked');
				$(this).parent().parent().children('td').removeClass('ui-state-highlight');
			});
		}
	});

	$('input[id=sub-dialog-idxCheck]').on('click', function() {

		var isTopCheck = true;
		var isChecked = $(this).prop('checked');
		var isTopChecked = $('#sub-dialog-topCheck').prop('checked');
		if (isChecked) {
			$('input[id=sub-dialog-idxCheck]').each(function () {
				if (!$(this).prop('checked')) {
					isTopCheck = false;
				}
			});

			if (isTopCheck) {
				$('#sub-dialog-topCheck').attr('checked', 'checked');
			}

		} else {
			if (isTopChecked) {
				$('#sub-dialog-topCheck').removeAttr('checked');
			}
		}

		if ($(this).prop('checked')) {
			$(this).parent().parent().children('td').addClass('ui-state-highlight');
		} else {
			$(this).parent().parent().children('td').removeClass('ui-state-highlight');
		}
	});

	/* Workspace */
	$('#workspace-but').on('click', function() {
		var workspace = $('#workspace').attr('value');
		setWorkSpace(workspace);
	});

	/* Local File */
	$('#local-file-but').on('click', function() {
		var fileName = $('#local-file').attr('value');
		setFile(fileName);
	});
});

window.onerror = function myErrorFunction(message, url, linenumber) {
	alert('A customized error message' + '\n'
			+ 'message : '+ message + '\n'
			+ 'url : ' + url + '\n'
			+ 'linenumber : ' + linenumber);
	return true;
};

/* Table mouse hover */
(function ($) {
	$.fn.styleTable = function (options) {
		var defaults = {
				css: 'ui-widget-content ui-grid-head-table'
		};
		options = $.extend(defaults, options);

		return this.each(function () {

			input = $(this);
			input.addClass(options.css);

			input.find('tr').on('mouseover mouseout', function (event) {
				if (event.type == 'mouseover') {
					$(this).children('td').addClass('ui-state-hover');
				} else {
					$(this).children('td').removeClass('ui-state-hover');
				}
			});

			input.find('td').addClass('ui-state-default');
		
		});
	};
})(jQuery);

/**
 * APPLET Workspace
 */
function setWorkSpace(workspace) {

	showLoading();
	var dir = document.getElementById('applet-dir-chooser');
	dir.getFileChooser(workspace);
}

/**
 * APPLET File Name
 */
function setFile(workspace) {

	showLoading();
	var fileApple = document.getElementById('applet-file-chooser');
	fileApple.getFileChooser(workspace);
}

/**
 * APPLET Setting Local Workspace
 */
function setLocalWorkspace(workspace) {

	$('#workspace').attr('value', workspace);
	$('#workspace').attr('title', workspace);
	window.status='';
	hideLoading();
}

/**
 * APPLET Setting Local File
 */
function setLocalFile(workspace) {

	$('#local-file').attr('value', workspace);
	$('#local-file').attr('title', workspace);
	window.status='';
	hideLoading();
}

/**
* Show of Loading
*/
function showLoading() {
	$('#loading').show();
}

/**
* Hide of Loading
*/
function hideLoading() {
	$('#loading').hide();
}

/**
 * Initialization Setting Dimensions Execute Result Message
 */
function initSetDimExecResultMsg() {

	$('td[id=result-msg]').each(function (i) {
		$(this).text('');
		$(this).attr('title', '');
	});
}

/**
 * Setting Dimensions Execute Result Message
 */
function setDimExecResultMsg(idx, code, msg) {

	$('td[id=result-msg]').each(function (i) {
		if (i == idx) {
			$(this).text(code);
			$(this).attr('title', msg);
			$(this).focus();
		}
	});
}

/**
 * Attributes JSON
 */
function getAttributesJson(attributeTableId) {

	var attributesJson = '';
	var idVar;
	var typeVar;
	var attrMulti = '';
	var attrValue = '';
	var attrIdVar = '';
	var attrValueVar = '';
	var attrValueMultiVar = '';

	var size = $('#' + attributeTableId + ' * td > [id]').not('.readonly').not('.disabled').size();

	if (size > 0) {
		attributesJson += '{';
		attributesJson += '"attributes":';
		attributesJson += '[';

		$('#' + attributeTableId + ' * td > [id]').not('.readonly').not('.disabled').each(function (i) {
			idVar = $(this).attr('id');
			typeVar = $(this).attr('type');
			attrValue = '';
			attrMulti = '';

			if (typeVar == 'checkbox') {

				attrValue = '';
				if ($(this).prop('checked')) {
					attrValue = 'Y';
				} else {
					attrValue = 'N';
				}
				attrMulti = 'N';
			} else {

				attrValue = $(this).attr('value');
				attrMulti = 'N';
			}

			attrIdVar = '"name":"' + idVar + '"';
			attrValueVar = '"value":"' + getAttrJsonFormet(attrValue) + '"';
			attrValueMultiVar = '"multi":"' + getAttrJsonFormet(attrMulti) + '"';
			attributesJson += '{' + attrIdVar + ',' + attrValueVar + ', ' + attrValueMultiVar + '}';

			if (size != (i+1)) {
				attributesJson += ',';
			}
		});

		attributesJson += ']';
		attributesJson += '}';
	}

	return attributesJson;
}

function getAssignSelectGroupIdJson(groupId) {

	var groupsJson = '';

	groupsJson += '{';
	groupsJson += '"groups":';
	groupsJson += '[';

	$('#' + groupId + ' > option:selected').each(function(i) {
		var value = $(this).attr('value');
		var groupIdVar = '"groupId":"' + value + '"';
		groupsJson += '{' + groupIdVar + '},';
	});

	groupsJson += ']';
	groupsJson += '}';

	return groupsJson;
}

function getRemoveSelectGroupIdJson(groupId) {

	var groupsJson = '';

	groupsJson += '{';
	groupsJson += '"groups":';
	groupsJson += '[';

	$('#' + groupId + ' > option:selected').each(function(i) {
		var value = $(this).attr('value');
		var groupIdVar = '"groupId":"' + value + '"';
		groupsJson += '{' + groupIdVar + '},';
	});

	groupsJson += ']';
	groupsJson += '}';

	return groupsJson;
}

function getSelectGroupIdJson(groupId) {

	var groupsJson = '';

	groupsJson += '{';
	groupsJson += '"groups":';
	groupsJson += '[';

	$('#' + groupId + ' > option').each(function(i) {
		var value = $(this).attr('value');
		var groupIdVar = '"groupId":"' + value + '"';
		groupsJson += '{' + groupIdVar + '},';
	});

	groupsJson += ']';
	groupsJson += '}';

	return groupsJson;
}

function getGroupIdJson(groupId) {

	var groupsJson = '';

	groupsJson += '{';
	groupsJson += '"groups":';
	groupsJson += '[';

	var groupIdVar = '"groupId":"' + groupId + '"';
	groupsJson += '{' + groupIdVar + '}';

	groupsJson += ']';
	groupsJson += '}';

	return groupsJson;
}

function getAttrJsonFormet(str) {

	var attrData = '';
	if (str == null || str == undefined || str == '') {
		return '';
	}

	attrData = str.replace(/\\/g, '\/').replace(/\t/g,'\\t').replace(/\n/g,'\\n').replace(/\r/g,'\\r').replace(/\"/g,'\\"');

	return attrData;
}

function getResultHTML(code, msg, gubun) {

	var color = '';
	if (code == 'OK') {
		color = '#45B8EB';
	} else if (code == 'EE') {
		color = '#DB0F0F';
	}
	return '<p><span style="font: bold 12px Arial; color:#ffffff; width:100px; margin: 5px; background-color: ' + color + '">' + code + '</span><span> [' + gubun+'] : ' + msg + '</span></p>';
}

function getRelateChdocsJson(requestId) {

	var relateChdocsJson = '';

	if (requestId != '') {
		relateChdocsJson += '{';
		relateChdocsJson += '"chdocs":';
		relateChdocsJson += '[';

		requestIdVar = '"chdocId":"'+requestId+'"';
		relateChdocsJson += '{'+requestIdVar+'}';

		relateChdocsJson += ']';
		relateChdocsJson += '}';
	}

	return relateChdocsJson;
}

/**
 * Include File Extend JSON
 */
function getIncludeFileExtendJson() {

	var fileExtendJson = '';

	var size = $('#include-file-extend-table * td > [id=FILE_EXTEND]').size();

	if (size > 0) {

		fileExtendJson += '{';
		fileExtendJson += '"userFilters":';
		fileExtendJson += '[';

		$('#include-file-extend-table * td > [id=FILE_EXTEND]').each(function (i) {

			if ($(this).attr('value') != '') {
				var fileExtendIdVar = '"fileExtend":"' + $(this).attr('value') + '"';
				fileExtendJson += '{'+fileExtendIdVar+'}';

				if (size != (i+1)) {
					fileExtendJson += ',';
				}
			}
		});

		fileExtendJson += ']';
		fileExtendJson += '}';
	}

	if (fileExtendJson == '{"userFilters":[]}') {
		fileExtendJson = '';
	}

	return fileExtendJson;
}

/**
 * Exclude File Extend JSON
 */
function getExcludeFileExtendJson() {

	var fileExtendJson = '';

	var size = $('#exclude-file-extend-table * td > [id=FILE_EXTEND]').size();

	if (size > 0) {

		fileExtendJson += '{';
		fileExtendJson += '"userFilters":';
		fileExtendJson += '[';

		$('#exclude-file-extend-table * td > [id=FILE_EXTEND]').each(function (i) {

			if ($(this).attr('value') != '') {
				var fileExtendIdVar = '"fileExtend":"' + $(this).attr('value') + '"';
				fileExtendJson += '{'+fileExtendIdVar+'}';

				if (size != (i+1)) {
					fileExtendJson += ',';
				}
			}
		});

		fileExtendJson += ']';
		fileExtendJson += '}';
	}

	if (fileExtendJson == '{"userFilters":[]}') {
		fileExtendJson = '';
	}

	return fileExtendJson;
}